# Display Layout

Display Layout allows users to leverage Drupal's Layout API to configure
entity displays. Rather than simply choosing between "Content" and
"Disabled", users can define layouts with custom regions to sort
fields without requiring field groups.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/display_layout).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/display_layout).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to the "Manage Display" form for a content entity.
2. Navigate to the "Display layout settings" option at the bottom.
3. Select which layout the display mode should use.
4. Save the configuration.
5. The corresponding regions for that layout will appear as options to sort the fields.


## Maintainers

- Jay Huskins - [jayhuskins](https://www.drupal.org/u/jayhuskins)
